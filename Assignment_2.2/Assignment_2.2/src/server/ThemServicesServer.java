package server;

import commonStuff.Car;
import commonStuff.ITaxAndPrice;
import java.rmi.Naming;
import java.rmi.RMISecurityManager;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class ThemServicesServer extends UnicastRemoteObject implements ITaxAndPrice {
    
    public ThemServicesServer() throws RemoteException {
        super();
    }
    
    public static void main(String[] args) throws Exception {
        // assign a security manager, in the event that dynamic classes are loaded
        if (System.getSecurityManager() == null) {
            System.setSecurityManager(new RMISecurityManager());
            
            // create an instance of the ServicesServer
            ThemServicesServer server = new ThemServicesServer();
            
            // bind it with the RMI Registry
            Naming.bind("ITaxAndPrice", server);
            
        }
    }

    @Override
    public double computeTax(Car c) throws RemoteException {
        // if incorrect data for the equation
        if (c.getEngineCapacity() < 0) {
            throw new IllegalArgumentException("Engine Capacity > 0");
        }
        else {
            int sum;
            if (c.getPurchasingPrice() <= 1600)
                sum = 8;
            else
                if (c.getPurchasingPrice() <= 2000)
                    sum = 18;
                else
                    if (c.getPurchasingPrice() <= 2600)
                        sum = 72;
                    else
                        if (c.getPurchasingPrice() <= 3000)
                            sum = 144;
                        else
                            sum = 290;
            double tax = c.getEngineCapacity() / 200 * sum;
            return tax;
        }
    }

    @Override
    public double computeSellingPrice(Car c) throws RemoteException {
        if (c.getPurchasingPrice() < 0) {
            throw new IllegalArgumentException("Are you kiddin' me? Purchasing price !< 0");
        }
        if (c.getFabriactionYear() < 1980 || c.getFabriactionYear() > 2014) {
            throw new IllegalArgumentException("Fabrication year has to be in [1980, 2014]");
        }
        
        return c.getPurchasingPrice() - c.getPurchasingPrice() / 7 * (2015 - c.getFabriactionYear());
    }
    
}
