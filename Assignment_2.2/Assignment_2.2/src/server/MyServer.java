package server;

import java.rmi.Naming;

public class MyServer {
    
    public static void main(String[] args) {
        System.out.println("Running Server");
        try {
            ThisComputes thisComputes = new ThisComputes();
            Naming.rebind("rmi://localhost:5000/calculateForMe", thisComputes);
        } catch(Exception e) {
            System.out.println("Problem in MyServer: " + e.toString());
        }
    }
}
