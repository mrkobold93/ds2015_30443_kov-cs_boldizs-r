package commonStuff;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ITaxAndPrice extends Remote {
    
    /**
     * Computes the tax for a car
     * @param c the car
     * @return the tax
     */
    public double computeTax(Car c) throws RemoteException;
    
    /**
     * Computes the estimated selling price for a car
     * @param c the car
     * @return the estimated selling price
     */
    public double computeSellingPrice(Car c) throws RemoteException;
    
    
}
