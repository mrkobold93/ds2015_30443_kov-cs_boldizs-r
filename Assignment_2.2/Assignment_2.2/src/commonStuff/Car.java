package commonStuff;

public class Car {
    
    private int fabriactionYear;
    private int engineCapacity;
    private double purchasingPrice;
    
    public Car() { }
    
    public Car(int fabricationYear, int engineCapacity, double purchasingPrice) {
        this.fabriactionYear = fabricationYear;
        this.engineCapacity = engineCapacity;
        this.purchasingPrice = purchasingPrice;
    }

    public int getFabriactionYear() {
        return fabriactionYear;
    }

    public void setFabriactionYear(int fabriactionYear) {
        this.fabriactionYear = fabriactionYear;
    }

    public int getEngineCapacity() {
        return engineCapacity;
    }

    public void setEngineCapacity(int engineCapacity) {
        this.engineCapacity = engineCapacity;
    }

    public double getPurchasingPrice() {
        return purchasingPrice;
    }

    public void setPurchasingPrice(double purchasingPrice) {
        this.purchasingPrice = purchasingPrice;
    }
    
}
