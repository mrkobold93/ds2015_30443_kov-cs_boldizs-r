<html>
<body>
<h2>Login</h2>

<form action="login" method="POST">
    <table>
        <tr>
            <td>Username</td>
            <td><input type="text" name="username"></td>
        </tr>
        <tr>
            <td>Password</td>
            <td><input type="password" name="password"></td>
        </tr>
    </table>
    <input type="submit" value="Log in">
</form>

<% if (request.getAttribute("loginResult") != null){
    if ((Boolean)request.getAttribute("loginResult") == true){
        out.println("Success.");
    }
    else{
        out.print("Incorrect username or password.");
    }
} %>
</body>
</html>
