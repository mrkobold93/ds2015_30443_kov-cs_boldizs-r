package com.mywebapp.hello.entity;

import java.util.List;
import java.util.Set;

/**
 * Created by andras on 16.10.2015.
 */
public class City {
    private int id;
    private String name;
    private float latitude;
    private float longitude;
    private Set flights;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public Set getFlights() {
        return flights;
    }

    public void setFlights(Set flights) {
        this.flights = flights;
    }
}
