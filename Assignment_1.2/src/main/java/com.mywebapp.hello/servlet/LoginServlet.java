package com.mywebapp.hello.servlet;

import com.mywebapp.hello.entity.City;
import com.mywebapp.hello.service.FlightService;
import com.mywebapp.hello.service.UserService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by andras on 16.10.2015.
 */
public class LoginServlet extends HttpServlet{

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        UserService userService = new UserService();
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        boolean loginResult = userService.login(username, password);
        request.setAttribute("loginResult", loginResult);
    }
}