package com.mywebapp.hello.service;

import com.mywebapp.hello.dao.UserDAO;
import com.mywebapp.hello.entity.User;
import org.hibernate.cfg.Configuration;

/**
 * Created by andras on 16.10.2015.
 */
public class UserService{

    public boolean login(String username, String password) {

        UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());
        User user = userDAO.getUserByUsernameAndPassword(username, password);
        return user == null ? false : true;

    }


}
