package com.mywebapp.hello.service;

import com.mywebapp.hello.dao.FlightDao;
import com.mywebapp.hello.entity.Flight;
import org.hibernate.cfg.Configuration;

import java.util.List;

/**
 * Created by andras on 16.10.2015.
 */
public class FlightService {

    private FlightDao flightDao = new FlightDao(new Configuration().configure().buildSessionFactory());

    public List<Flight> getAll(){
        return flightDao.getAll();
    }

}
