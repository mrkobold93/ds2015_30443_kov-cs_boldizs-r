package com.mywebapp.hello.dao;

import com.mywebapp.hello.entity.Flight;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by andras on 16.10.2015.
 */
public class FlightDao {

    private SessionFactory factory;

    public FlightDao(SessionFactory factory) {
        this.factory = factory;
    }

    public List<Flight> getAll(){
        Session session = factory.openSession();
        Query q = session.createQuery("from Flight");
        return q.list();
    }
}
