package com.mywebapp.hello.dao;

import com.mywebapp.hello.entity.User;
import org.hibernate.*;
import org.hibernate.criterion.Expression;

import java.util.List;

/**
 * Created by andras on 16.10.2015.
 */
public class UserDAO{
    private SessionFactory factory;

    public UserDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public User getUserByUsernameAndPassword(String username, String password) {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Expression.eq("username", username));
        criteria.add(Expression.eq("password", password));
        return (User)criteria.uniqueResult();
    }


}
