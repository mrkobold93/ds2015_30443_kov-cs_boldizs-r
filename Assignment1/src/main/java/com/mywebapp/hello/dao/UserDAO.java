package com.mywebapp.hello.dao;

import com.mywebapp.hello.entity.User;
import org.hibernate.*;
import org.hibernate.criterion.Expression;

import javax.swing.*;
import java.util.List;

/**
 * Created by Mr Kobold on 10/24/2015.
 */
public class UserDAO {

    private SessionFactory factory;

    public UserDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public String validateLogin(String username, String password) {
        Session session = factory.openSession();
        try {
            // fetch all users data from DB
            List users = session.createQuery("from User").list();
            // cycle through fetched data, and check where does the username equal the introduced value
            for (Object u : users) {
                User currentUser = (User) u;
                if (currentUser.getUsername().equals(username) && currentUser.getPassword().equals(password)) {
                    return currentUser.getRole();
                }
            }
        } catch(Exception e) {
            System.out.println("Problem" + e.toString());
            return null;
        }
        return null;
    }
}
