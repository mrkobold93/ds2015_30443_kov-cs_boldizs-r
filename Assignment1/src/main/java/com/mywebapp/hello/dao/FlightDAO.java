package com.mywebapp.hello.dao;

import com.mywebapp.hello.entity.Flight;
import jdk.nashorn.internal.scripts.JO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import org.hibernate.Transaction;

import org.hibernate.Query;
import org.hibernate.criterion.Restrictions;

import javax.swing.*;
import java.util.List;

/**
 * Created by Mr Kobold on 10/25/2015.
 */
public class FlightDAO {

    private SessionFactory factory;

    private Session saved_session;

    public FlightDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public Flight[] getFlightsList() {
        Session session = factory.openSession();
        try {
            // get all Flights into a list first
            List flightsList = session.createQuery("from Flight").list();
            // then put them into an array[] of Flights
            Flight[] flights = new Flight[flightsList.size()];
            // now move flights from the list into the array
            for (int i = 0; i < flightsList.size(); i++) {
                flights[i] = (Flight) flightsList.get(i);
            }
            // now return the resulting array
            return flights;
        } catch (Exception e) {
            String problem;
            JOptionPane.showMessageDialog(null, "Error in getFlightsList():" + e.toString());
            //System.out.println("Exception while getting list of flights int FlightDAO");
            return null;
        }
    }

    public void insertFlightInotDB(Flight f) {

        Session session;
        if (saved_session != null){
            session = saved_session;
        }
        else {
            session = factory.openSession();
        }
        try {
            Transaction transaction = session.beginTransaction();
            session.save(f);
            transaction.commit();
        } catch (Exception ex) {
            System.out.println("Error while inserting flight into DB");
        } finally {
            session.flush();
            session.close();
            saved_session = null;
        }
    }

    public void updateFlight(int f) {
        try {
            Session session = factory.openSession();
            Transaction transaction = session.getTransaction();
            transaction.begin();
            Flight flight = (Flight)session.load(Flight.class, f);
            session.merge(flight);
            transaction.commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Problem");

        }
    }

    public int getNoOfFlights() {
        Session session = factory.openSession();
        try {
            // put flights in a list
            List flightsList = session.createQuery("from Flight").list();
            // return the size of the list
            return flightsList.size();
        } catch (Exception ex) {
            return -1;
        }
    }

    public String getLocalTimes(int i) {
        Session session = factory.openSession();
        try {
            // get flights
            List flightsList = session.createQuery("from Flight").list();
            Flight awesomeFlight = (Flight) flightsList.get(i);
            return "This flight departures at: " + awesomeFlight.getDeparture_time() + "local time,\nAnd arrives at: " + awesomeFlight.getArrival_time() + " local time";
        } catch (Exception e) {
            return null;
        }
    }

    public void deleteFlight(int id) {
        try {
            Session session = factory.openSession();
            Transaction tx = session.beginTransaction();
            Query query = session.createQuery("delete Flight where flight_no = :flno");
            query.setParameter("flno", id);
            int result = query.executeUpdate();
            session.flush();
            tx.commit();
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }

    public Flight getFlightByPosition(int pos) {
        Flight[] flights = getFlightsList();
        return flights[pos];
    }

    public Flight loadFlight(int id) {
        Session session = factory.openSession();
        saved_session = session;
        return (Flight)session.load(Flight.class, id);

    }
}
