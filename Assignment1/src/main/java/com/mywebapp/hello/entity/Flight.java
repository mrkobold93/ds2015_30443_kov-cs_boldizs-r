package com.mywebapp.hello.entity;

import java.util.Date;

/**
 * Created by Mr Kobold on 10/23/2015.
 */
public class Flight {

    private int flight_no;
    private String airplane_type;
    private java.util.Date departure_time, arrival_time;
    /*
    @ManyToOne
    @JoinColumn(name = "departure_city", referencedColumnName = "idcities", nullable = true, insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)*/
    //private City departure_city;

    /*@ManyToOne
    @JoinColumn(name = "arrival_city", referencedColumnName = "idcities", nullable = true, insertable = false, updatable = false)
    @NotFound(action = NotFoundAction.IGNORE)*/
    //private City arrival_city;

    private int departure_city, arrival_city;

    public int getDeparture_city() {
        return departure_city;
    }

    public void setDeparture_city(int departure_city) {
        this.departure_city = departure_city;
    }

    public int getArrival_city() {
        return arrival_city;
    }

    public void setArrival_city(int arrival_city) {
        this.arrival_city = arrival_city;
    }

    public Date getDeparture_time() {
        return departure_time;
    }

    public void setDeparture_time(Date departure_time) {
        this.departure_time = departure_time;
    }

    public Date getArrival_time() {
        return arrival_time;
    }

    public void setArrival_time(Date arrival_time) {
        this.arrival_time = arrival_time;
    }

    public int getFlight_no() {
        return flight_no;
    }

    public void setFlight_no(int flight_no) {
        this.flight_no = flight_no;
    }

    public String getAirplane_type() {
        return airplane_type;
    }

    public void setAirplane_type(String airplane_type) {
        this.airplane_type = airplane_type;
    }


}
