package com.mywebapp.hello.servlet;

import com.mywebapp.hello.dao.FlightDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by Mr Kobold on 10/25/2015.
 */
public class UserServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
       // RequestDispatcher requestDispatcher = request.getRequestDispatcher("/user.jsp");
        //requestDispatcher.forward(request, response);

        // get number of flight
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());

        // get no of flights
        int noOfFlights = flightDAO.getNoOfFlights();

        // check which button was pressed
        String buttonName = "";
        for (int i = 0; i < noOfFlights; i++) {
            buttonName = "button" + i;
            if (request.getParameter(buttonName) != null) {
                //JOptionPane.showMessageDialog(null, flightDAO.getLocalTimes(i));
                response.getWriter().print(flightDAO.getLocalTimes(i));
            }
        }
    }


}
