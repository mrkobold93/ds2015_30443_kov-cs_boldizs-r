package com.mywebapp.hello.servlet;

import com.mywebapp.hello.dao.FlightDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;

/**
 * Created by Mr Kobold on 10/25/2015.
 */
public class AdminServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response) {
        try {
            String action = request.getParameter("action");
            if (action.equals("delete")) {
                // get the Id of the flight intended to delete
                int id = Integer.parseInt(request.getParameter("id"));

                // ask FlightDAO to delete
                FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
                flightDAO.deleteFlight(id);

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin.jsp");
                requestDispatcher.forward(request, response);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.toString());
        }
    }
}
