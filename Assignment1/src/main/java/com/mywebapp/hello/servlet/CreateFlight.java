package com.mywebapp.hello.servlet;

import com.mywebapp.hello.dao.FlightDAO;
import com.mywebapp.hello.entity.Flight;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transaction;
import java.io.IOException;

/**
 * Created by Mr Kobold on 10/27/2015.
 */
public class CreateFlight extends HttpServlet {
    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        // formatter for times
        DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy/MM/dd HH:mm:ss");
        // create empty Flight object
        Flight flight = new Flight();
        // fill it with data gotten from the Form
        flight.setAirplane_type(request.getParameter("airplaneType"));
        //flight.setArrival_city(Integer.parseInt(request.getParameter("arrivalCity")));
        flight.setArrival_time(formatter.parseDateTime(request.getParameter("arrivalTime")).toDate());
        //flight.setDeparture_city(Integer.parseInt(request.getParameter("departureCity")));
        flight.setDeparture_time((formatter.parseDateTime(request.getParameter("departureTime")).toDate()));
        // insert this into the database
        FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
        flightDAO.insertFlightInotDB(flight);

        RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin.jsp");
        requestDispatcher.forward(request, response);
    }
}
