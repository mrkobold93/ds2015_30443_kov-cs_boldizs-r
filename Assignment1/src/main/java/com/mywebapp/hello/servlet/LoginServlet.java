package com.mywebapp.hello.servlet;

import com.mywebapp.hello.dao.UserDAO;
import org.hibernate.cfg.Configuration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by Mr Kobold on 10/24/2015.
 */

public class LoginServlet extends HttpServlet {

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        UserDAO userDAO = new UserDAO(new Configuration().configure().buildSessionFactory());

        String login_result = userDAO.validateLogin(username, password);

        if (login_result.equals("admin")) {
            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin.jsp");
            requestDispatcher.forward(request, response);
        }
        else
        {
            if (login_result.equals("user")) {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/user.jsp");
                requestDispatcher.forward(request, response);
            }
            else {
                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/idiot.jsp");
                requestDispatcher.forward(request, response);
            }
        }
    }
}
