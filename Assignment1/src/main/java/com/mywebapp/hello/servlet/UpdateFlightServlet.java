package com.mywebapp.hello.servlet;

import com.mywebapp.hello.dao.CityDAO;
import com.mywebapp.hello.dao.FlightDAO;
import com.mywebapp.hello.entity.Flight;
import org.hibernate.cfg.Configuration;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.swing.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 * Created by Mr Kobold on 10/31/2015.
 */
public class UpdateFlightServlet extends HttpServlet {

    FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

        if (request.getParameter("the_action") != null && request.getParameter("the_action").equals("update_the_flight")) {
            try {
                int id_of_updated_flight = Integer.parseInt(request.getParameter("flight_no"));

                Flight modified_flight = flightDAO.loadFlight(id_of_updated_flight);

                // formatter for times
                SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                // set attributes for the flight
                modified_flight.setDeparture_time(format.parse(request.getParameter("departure_time")));
                modified_flight.setArrival_time(format.parse(request.getParameter("arrival_time")));
                modified_flight.setDeparture_city(Integer.parseInt(request.getParameter("departure_city")));
                modified_flight.setArrival_city(Integer.parseInt(request.getParameter("arrival_city")));
                modified_flight.setAirplane_type(request.getParameter("airplane_type"));

                flightDAO.updateFlight(modified_flight.getFlight_no());

                RequestDispatcher requestDispatcher = request.getRequestDispatcher("/admin.jsp");
                requestDispatcher.forward(request, response);

            } catch (Exception ex) {

            }

        } else {

            // get number of flight
            FlightDAO flightDAO = new FlightDAO(new Configuration().configure().buildSessionFactory());
            CityDAO cityDAO = new CityDAO(new Configuration().configure().buildSessionFactory());

            // we have to load the stuff from flight [i] into the form
            Flight updated_flight = flightDAO.loadFlight(Integer.parseInt(request.getParameter("update_id")));
            request.setAttribute("departure_city", cityDAO.getByID(updated_flight.getDeparture_city()).getName());
            request.setAttribute("arrival_city", cityDAO.getByID(updated_flight.getArrival_city()).getName());
            request.setAttribute("departure_time", updated_flight.getDeparture_time());
            request.setAttribute("arrival_time", updated_flight.getArrival_time());
            request.setAttribute("airplane_type", updated_flight.getAirplane_type());
            request.setAttribute("flight_no", updated_flight.getFlight_no());

            RequestDispatcher requestDispatcher = request.getRequestDispatcher("/update.jsp");
            requestDispatcher.forward(request, response);
        }
    }

}
