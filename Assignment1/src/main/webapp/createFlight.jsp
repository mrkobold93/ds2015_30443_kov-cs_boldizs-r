
<html>
    <head>
        <title>Create New Flight</title>
    </head>
    <body>
        <h2>Create Flight</h2>
        <br>
        <b>Fill out this form</b>
        <br><br>
        <form action="createFlight" method="POST">
            <table>
                <tr>
                    <td>Departure City</td>
                    <td><input type="text" name="departureCity"/><td>
                </tr>
                <tr>
                    <td>Arrival City</td>
                    <td><input type="text" name="arrivalCity"/></td>
                </tr>
                <tr>
                    <td>Departure Time</td>
                    <td><input type="datetime" name="departureTime"/></td>
                </tr>
                <tr>
                    <td>Arrival Time</td>
                    <td><input type="datetime" name="arrivalTime"/></td>
                <tr>
                    <td>Airplane Type</td>
                    <td><input type="text" name="airplaneType"/></td>
                </tr>
                <tr>
                    <td></td>
                    <td><input type="submit" value="Create"/><input type="reset" value="Reset"/></td>
                </tr>
            </table>
        </form>
    </body>
</html>