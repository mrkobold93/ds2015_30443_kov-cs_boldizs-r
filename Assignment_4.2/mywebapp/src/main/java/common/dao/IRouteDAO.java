package common.dao;

import common.entity.Route;

import java.util.List;

public interface IRouteDAO {
    public void persist (Route route);

    public List<Route> getRoutesForPackage (int packageId);
}
