package common.dao;

import common.entity.User;

import java.util.List;

public interface IUserDAO {
    public User login (String username, String password);

    public List<User> getAll ();

    public User load (int id);

    public void register (String username, String password);
}
