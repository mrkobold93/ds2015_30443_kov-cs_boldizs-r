package common.dao;

import common.entity.City;

import java.util.List;

public interface ICityDAO {
    public List<City> getAll();

    public City load (int id);
}
