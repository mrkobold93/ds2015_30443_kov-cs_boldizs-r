package common.dao;

import common.entity.City;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import java.util.List;

public class CityDAO implements ICityDAO {
    private SessionFactory factory = MySessionFactory.getSessionFactory();

    @Override
    public List<City> getAll() {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(City.class);
        List<City> cities = criteria.list();
        session.close();
        return cities;
    }

    @Override
    public City load(int id) {
        return (City)factory.openSession().load(City.class, id);
    }

}
