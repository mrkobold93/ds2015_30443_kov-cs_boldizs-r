package common.dao;

import common.entity.User;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Expression;

import java.util.List;

public class UserDAO implements IUserDAO {
    private SessionFactory factory = MySessionFactory.getSessionFactory();

    @Override
    public User login(String username, String password) {
        System.out.println("Logging in with the credentials: username: " + username + ", password: " + password);
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(User.class);
        criteria.add(Expression.eq("username", username));
        criteria.add(Expression.eq("password", password));
        User user = (User) criteria.uniqueResult();
        session.close();
        if (user == null) {
            return new User();
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(User.class);
        List<User> users = criteria.list();
        session.close();
        return users;
    }

    @Override
    public User load(int id) {
        return (User)factory.openSession().load(User.class, id);
    }

    @Override
    public void register(String username, String password) {
        User user = new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setIsAdmin(false);

        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(user);
        tx.commit();
        session.flush();
        session.close();
    }
}
