package common.dao;

import common.entity.*;
import common.entity.Package;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;

import java.util.List;

public class PackageDAO implements IPackageDAO {
    private SessionFactory factory = MySessionFactory.getSessionFactory();

    @Override
    public List<Package> getAll() {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(common.entity.Package.class);
        List<common.entity.Package> packages = criteria.list();
        session.close();
        return packages;
    }

    @Override
    public void persist(Package packagee) {
        Session session = factory.openSession();
        Transaction tx = session.beginTransaction();
        session.save(packagee);
        tx.commit();
        session.flush();
        session.close();
    }

    @Override
    public void delete(int id) {
        Session session = factory.openSession();
        Package packagee = (Package)session.load(Package.class, id);
        Transaction tx = session.beginTransaction();
        session.delete(packagee);
        tx.commit();
        session.flush();
        session.close();
    }

    @Override
    public void track(int id) {
        Session session = factory.openSession();
        Package packagee = (Package)session.load(Package.class, id);
        Transaction tx = session.beginTransaction();
        packagee.setTracking(!packagee.getTracking());
        tx.commit();
        session.flush();
        session.close();
    }

    @Override
    public Package load(int id) {
        return (Package)factory.openSession().load(Package.class, id);
    }

    @Override
    public List<Package> getPackagesForUser(int userId) {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(common.entity.Package.class);
        Criterion userCriterion = Restrictions.eq("id", userId);
        criteria.createCriteria("sender").add(userCriterion);

        List<common.entity.Package> packages = criteria.list();
        session.close();
        return packages;
    }

    @Override
    public List<Package> searchPackage(String searchString) {
        Session session = factory.openSession();
        Criteria criteria = session.createCriteria(common.entity.Package.class);
        criteria.add(Restrictions.like("name", searchString, MatchMode.ANYWHERE));
        List<common.entity.Package> packages = criteria.list();
        session.close();
        return packages;
    }
}
