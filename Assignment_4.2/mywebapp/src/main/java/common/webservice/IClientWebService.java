package common.webservice;

import common.entity.Route;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;

@WebService
@SOAPBinding(style = SOAPBinding.Style.RPC)
public interface IClientWebService {
    @WebMethod
    public common.entity.Package[] getOwnPackages(int userId);

    @WebMethod
    public common.entity.Package[] getSearchResults (String searchString);

    @WebMethod
    public Route[] getRoutesForPackage (int id);
}
