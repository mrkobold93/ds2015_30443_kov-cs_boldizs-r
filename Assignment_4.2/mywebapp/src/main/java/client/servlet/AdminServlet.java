package client.servlet;

import client.webservice.ServiceLocator;
import common.entity.City;
import common.entity.User;
import common.vo.PackageVO;
import common.vo.StatusVO;
import common.webservice.IAdminWebService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class AdminServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        if (action.equals("add")) {
            PackageVO packageVO = new PackageVO();
            packageVO.setSender(Integer.parseInt(request.getParameter("sender")));
            packageVO.setReceiver(Integer.parseInt(request.getParameter("receiver")));
            packageVO.setName(request.getParameter("name"));
            packageVO.setDescription(request.getParameter("description"));
            packageVO.setSenderCity(Integer.parseInt(request.getParameter("senderCity")));
            packageVO.setDestinationCity(Integer.parseInt(request.getParameter("destinationCity")));
            packageVO.setTracking((request.getParameter("tracking").equals("on")));
            ServiceLocator.getIAdminWebService().savePackage(packageVO);
        }
        else if (action.equals("updateStatus")) {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm");
            StatusVO statusVO = new StatusVO();
            statusVO.setPackagee(Integer.parseInt(request.getParameter("package")));
            statusVO.setCity(Integer.parseInt(request.getParameter("city")));
            try {
                statusVO.setDate(format.parse(request.getParameter("date")));
            } catch (ParseException e) {
                statusVO.setDate(new Date());
            }
            ServiceLocator.getIAdminWebService().updateStatus(statusVO);
        }
        forward(request, response);

    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        int id = Integer.parseInt(request.getParameter("id"));
        if (action.equals("delete")) {
            ServiceLocator.getIAdminWebService().deletePackage(id);
        } else if (action.equals("track")) {
            ServiceLocator.getIAdminWebService().trackPackage(id);
        }
        forward(request, response);
    }

    private void forward(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        IAdminWebService IAdminWebService = ServiceLocator.getIAdminWebService();
        User[] users = IAdminWebService.getAllUsers();
        City[] cities = IAdminWebService.getAllCities();
        common.entity.Package[] packages = IAdminWebService.getAllPackages();
        request.setAttribute("users", users);
        request.setAttribute("cities", cities);
        request.setAttribute("packages", packages);
        request.getRequestDispatcher("/admin.jsp").forward(request, response);
    }
}
