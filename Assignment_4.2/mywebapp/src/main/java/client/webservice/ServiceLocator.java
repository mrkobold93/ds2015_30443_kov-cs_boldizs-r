package client.webservice;

import common.webservice.IAdminWebService;
import common.webservice.IClientWebService;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class ServiceLocator {
    private static common.webservice.IAdminWebService IAdminWebService = null;
    private static common.webservice.IClientWebService IClientWebService = null;

    public static IAdminWebService getIAdminWebService() {
        if (IAdminWebService == null) {
            URL wsdlUrl = null;
            try {
                wsdlUrl = new URL("http://localhost:8086/adminwebservice/admin?wsdl");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            QName qname = new QName("http://webservice.adminWS/", "AdminWebServiceImplService");
            Service service = Service.create(wsdlUrl, qname);
            IAdminWebService = service.getPort(IAdminWebService.class);
        }
        return IAdminWebService;
    }
    public static IClientWebService getIClientWebService() {
        if (IClientWebService == null) {
            URL wsdlUrl = null;
            try {
                wsdlUrl = new URL("http://localhost:8085/clientwebservice/servlet?wsdl");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            QName qname = new QName("http://webservice.clientWS/", "ClientWebServiceImplService");
            Service service = Service.create(wsdlUrl, qname);
            IClientWebService = service.getPort(IClientWebService.class);
        }
        return IClientWebService;
    }
}
