package clientWS.webservice;

import common.dao.*;
import common.entity.*;
import common.entity.Package;
import common.webservice.IClientWebService;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "common.webservice.IClientWebService")
public class ClientWebService implements IClientWebService {

    common.dao.IUserDAO IUserDAO = new UserDAO();
    common.dao.ICityDAO ICityDAO = new CityDAO();
    common.dao.IPackageDAO IPackageDAO = new PackageDAO();
    IRouteDAO IRouteDAO = new RouteDAO();


    @Override
    public common.entity.Package[] getOwnPackages(int userId) {
        List<Package> packages = IPackageDAO.getPackagesForUser(userId);
        return packages.toArray(new Package[packages.size()]);
    }

    @Override
    public Package[] getSearchResults(String searchString) {
        List<Package> searchResults = IPackageDAO.searchPackage(searchString);
        return searchResults.toArray(new Package[searchResults.size()]);
    }

    @Override
    public Route[] getRoutesForPackage(int id) {
        List<Route> routes = IRouteDAO.getRoutesForPackage(id);
        return routes.toArray(new Route[routes.size()]);
    }

}
