package adminWS.webservice;

import common.dao.*;
import common.entity.*;
import common.entity.Package;
import common.vo.PackageVO;
import common.vo.StatusVO;
import common.webservice.IAdminWebService;

import javax.jws.WebService;
import java.util.List;

@WebService(endpointInterface = "common.webservice.IAdminWebService")
public class AdminWebService implements IAdminWebService {

    common.dao.IUserDAO IUserDAO = new UserDAO();
    common.dao.ICityDAO ICityDAO = new CityDAO();
    common.dao.IPackageDAO IPackageDAO = new PackageDAO();
    IRouteDAO IRouteDAO = new RouteDAO();

    @Override
    public User login(String username, String password) {
        return IUserDAO.login(username, password);
    }

    @Override
    public User[] getAllUsers() {
        List<User> users = IUserDAO.getAll();
        return users.toArray(new User[users.size()]);
    }

    @Override
    public City[] getAllCities() {
        List<City> cities = ICityDAO.getAll();
        return cities.toArray(new City[cities.size()]);
    }

    @Override
    public common.entity.Package[] getAllPackages() {
        List<common.entity.Package> packages = IPackageDAO.getAll();
        return packages.toArray(new Package[packages.size()]);
    }

    @Override
    public void savePackage(PackageVO packageVO) {
        Package packagee = new Package();
        packagee.setSender(IUserDAO.load(packageVO.getSender()));
        packagee.setReceiver(IUserDAO.load(packageVO.getReceiver()));
        packagee.setName(packageVO.getName());
        packagee.setDescription(packageVO.getDescription());
        packagee.setSenderCity(ICityDAO.load(packageVO.getSenderCity()));
        packagee.setDestinationCity(ICityDAO.load(packageVO.getDestinationCity()));
        packagee.setTracking(packageVO.getTracking());

        IPackageDAO.persist(packagee);
    }

    @Override
    public void deletePackage(int id) {
        IPackageDAO.delete(id);
    }

    @Override
    public void trackPackage(int id) {
        IPackageDAO.track(id);
    }

    @Override
    public void updateStatus(StatusVO statusVO) {
        Route route = new Route();
        route.setPackagee(IPackageDAO.load(statusVO.getPackagee()));
        route.setCity(ICityDAO.load(statusVO.getCity()));
        route.setTime(statusVO.getDate());
        IRouteDAO.persist(route);
    }

    @Override
    public void register(String username, String password) {
        IUserDAO.register(username, password);
    }
}
