package adminWS.endpoint;

import adminWS.webservice.AdminWebService;

import javax.xml.ws.Endpoint;

public class AdminWebServicePublisher {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8086/adminwebservice/admin", new AdminWebService());
    }
}
