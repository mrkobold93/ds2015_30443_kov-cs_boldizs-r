<%--
  Created by IntelliJ IDEA.
  User: Arnold
  Date: 17/01/16
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="common.entity.*" %>
<%@ page import="common.entity.Package" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h2>Add package</h2>

<form action="admin" method="post">
    <input type="hidden" name="action" value="add">
    <table>
        <tr>
            <td>Sender</td>
            <td>
                <select name="sender">
                    <% for (User user : (User[]) request.getAttribute("users")) {%>
                    <option value="<% out.print(user.getId()); %>"><% out.print(user.getUsername()); %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td>Receiver</td>
            <td>
                <select name="receiver">
                    <% for (User user : (User[]) request.getAttribute("users")) {%>
                    <option value="<% out.print(user.getId()); %>"><% out.print(user.getUsername()); %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td>Name</td>
            <td><input type="text" name="name" required/></td>
        </tr>
        <tr>
            <td>Description</td>
            <td><input type="text" name="description" required></td>
        </tr>
        <tr>
            <td></td>
        </tr>
        <tr>
            <td>Sender city</td>
            <td>
                <select name="senderCity">
                    <% for (City city : (City[]) request.getAttribute("cities")) {%>
                    <option value="<% out.print(city.getId()); %>"><% out.print(city.getName()); %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td>Destination city</td>
            <td>
                <select name="destinationCity">
                    <% for (City city : (City[]) request.getAttribute("cities")) {%>
                    <option value="<% out.print(city.getId()); %>"><% out.print(city.getName()); %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td>Tracking</td>
            <td><input type="checkbox" name="tracking"></td>
        </tr>

    </table>
    <input type="submit" value="Add package">
</form>

<table border="1">
    <tr>
        <td>Id</td>
        <td>Sender</td>
        <td>Receiver</td>
        <td>Name</td>
        <td>Description</td>
        <td>Sender City</td>
        <td>Destination City</td>
        <td>Tracking</td>
        <td>Remove</td>
        <td>Track</td>
    </tr>
    <% for (Package packagee : (Package[]) request.getAttribute("packages")) { %>
    <tr>
        <td><% out.print(packagee.getId()); %></td>
        <td><% out.print(packagee.getSender().getUsername()); %></td>
        <td><% out.print(packagee.getReceiver().getUsername()); %></td>
        <td><% out.print(packagee.getName()); %></td>
        <td><% out.print(packagee.getDescription()); %></td>
        <td><% out.print(packagee.getSenderCity().getName()); %></td>
        <td><% out.print(packagee.getDestinationCity().getName()); %></td>
        <td><% out.print(packagee.getTracking());%></td>
        <td><a href="admin?action=delete&id=<%out.print(packagee.getId()); %>">Remove</a></td>
        <td><a href="admin?action=track&id=<%out.print(packagee.getId()); %>">Track/Untrack</a></td>
    </tr>
    <% } %>
</table>

<form action="admin" method="post">
    <input type="hidden" name="action" value="updateStatus">
    <table>
        <tr>
            <td>Package</td>
            <td><select name="package">
                <% for (Package packagee : (Package[]) request.getAttribute("packages")) {%>
                <option value="<% out.print(packagee.getId()); %>"><% out.print(packagee.getId()); %></option>
                <% } %>
            </select></td>
        </tr>
        <tr>
            <td>City</td>
            <td>
                <select name="city">
                    <% for (City city : (City[]) request.getAttribute("cities")) {%>
                    <option value="<% out.print(city.getId()); %>"><% out.print(city.getName()); %></option>
                    <% } %>
                </select>
            </td>
        </tr>
        <tr>
            <td>Time</td>
            <td><input type="text" name="date" required=""></td>
        </tr>
        <tr>
            <td>
                <input type="submit" value="Update status">
            </td>
        </tr>
    </table>
</form>

</body>
</html>
