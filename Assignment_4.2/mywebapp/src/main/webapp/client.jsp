<%--
  Created by IntelliJ IDEA.
  User: Arnold
  Date: 17/01/16
  Time: 15:01
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="common.entity.*" %>
<%@ page import="common.entity.Package" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h2>All packages</h2>
<table border="1">
    <tr>
        <td>Id</td>
        <td>Sender</td>
        <td>Receiver</td>
        <td>Name</td>
        <td>Description</td>
        <td>Sender City</td>
        <td>Destination City</td>
        <td>Tracking</td>
        <td>Remove</td>
        <td>Track</td>
    </tr>
    <% for (Package packagee : (Package[]) request.getAttribute("packages")) { %>
    <tr>
        <td><% out.print(packagee.getId()); %></td>
        <td><% out.print(packagee.getSender().getUsername()); %></td>
        <td><% out.print(packagee.getReceiver().getUsername()); %></td>
        <td><% out.print(packagee.getName()); %></td>
        <td><% out.print(packagee.getDescription()); %></td>
        <td><% out.print(packagee.getSenderCity().getName()); %></td>
        <td><% out.print(packagee.getDestinationCity().getName()); %></td>
        <td><% out.print(packagee.getTracking());%></td>
        <td><a href="admin?action=delete&id=<%out.print(packagee.getId()); %>">Remove</a></td>
        <td><a href="admin?action=track&id=<%out.print(packagee.getId()); %>">Track/Untrack</a></td>
    </tr>
    <% } %>
</table>

<h2>Search package by name</h2>

<form action="client" method="post">
    <input type="hidden" name="action" value="search">
    <table>
        <tr>
            <td>Name</td>
            <td><input type="text" name="searchName"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Search"></td>
        </tr>
    </table>
</form>
<% if (request.getAttribute("searchResults") != null) {%>
<table border="1">
    <tr>
        <td>Id</td>
        <td>Sender</td>
        <td>Receiver</td>
        <td>Name</td>
        <td>Description</td>
        <td>Sender City</td>
        <td>Destination City</td>
        <td>Tracking</td>
        <td>Remove</td>
        <td>Track</td>
    </tr>
    <% for (Package packagee : (Package[]) request.getAttribute("searchResults")) { %>
    <tr>
        <td><% out.print(packagee.getId()); %></td>
        <td><% out.print(packagee.getSender().getUsername()); %></td>
        <td><% out.print(packagee.getReceiver().getUsername()); %></td>
        <td><% out.print(packagee.getName()); %></td>
        <td><% out.print(packagee.getDescription()); %></td>
        <td><% out.print(packagee.getSenderCity().getName()); %></td>
        <td><% out.print(packagee.getDestinationCity().getName()); %></td>
        <td><% out.print(packagee.getTracking());%></td>
    </tr>
    <% } %>
</table>
<% } %>

<h2>Package status checking</h2>
<form action="client" method="post">
    <input type="hidden" name="action" value="checkStatus">
    <table>
        <tr>
            <td>Id</td>
            <td><input type="number" name="id"></td>
        </tr>
        <tr>
            <td><input type="submit" value="Check"></td>
        </tr>
    </table>
</form>

<% if (request.getAttribute("routes") != null) {%>
<table border="1">
    <tr>
        <td>Package Id</td>
        <td>City</td>
        <td>Time</td>
    </tr>
    <% for (Route route : (Route[]) request.getAttribute("routes")) { %>
    <tr>
        <td><% out.print(route.getPackagee().getId()); %></td>
        <td><% out.print(route.getCity().getName()); %></td>
        <td><% out.print(route.getTime()); %></td>

    </tr>
    <% } %>
</table>
<% } %>

</body>
</html>
