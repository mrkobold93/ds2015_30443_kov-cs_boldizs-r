package com.mrkobold.dsfinal.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = "com.mrkobold.dsfinal")
public class AppConfig {

}
