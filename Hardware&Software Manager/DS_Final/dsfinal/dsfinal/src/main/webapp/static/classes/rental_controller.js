'use strict';

App.controller('RentalController', ['$scope', 'RentalService', function($scope, RentalService) {
          var self = this;
          self.user={id:null,name:'',password:'',priviledge:''};
          self.users=[];
          self.car={id:null,brand:'',price:'',year:''};
          self.cars=[];
          self.rental={id:null,user:'',car:'',sdate:'',edate:''};
          self.rentals=[];
          
          self.fetchAllRentals = function(){
              RentalService.fetchAllRentals()
                  .then(
      					       function(d) {
      						        self.rentals = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
              
          self.fetchAllUsers = function(){
              RentalService.fetchAllUsers()
                  .then(
      					       function(d) {
      						        self.users = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
          self.fetchAllCars = function(){
              RentalService.fetchAllCars()
                  .then(
      					       function(d) {
      						        self.cars = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching Currencies');
            					}
      			       );
          };
           
          self.createRental = function(rental){
              RentalService.createRenal(rental)
		              .then(
                      self.fetchAllRentals, 
				              function(errResponse){
					               console.error('Error while creating User.');
				              }	
                  );
          };

         self.deleteRental = function(id){
              RentalService.deleteRental(id)
		              .then(
				              self.fetchAllRentals, 
				              function(errResponse){
					               console.error('Error while deleting User.');
				              }	
                  );
          };

          self.fetchAllUsers();
          self.fetchAllCars();
          self.fetchAllRentals();

          self.submit = function() {
              if(self.rental.id==null){
                  console.log('Saving New Rental', self.rental);    
                  self.createRental(self.rental);
              }
              self.reset();
          };
      
          self.remove = function(id){
              console.log('id to be deleted', id);
              for(var i = 0; i < self.rentals.length; i++){
                  if(self.rentals[i].id == id) {
                     self.reset();
                     break;
                  }
              }
              self.deleteRental(id);
          };

          
          self.reset = function(){
        	  self.rental={id:null,user:'',car:'',sdate:'',edate:''};
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
