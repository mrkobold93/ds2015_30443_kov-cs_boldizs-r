'use strict';
 
App.controller('LoginController', ['$scope','$location', 'LoginService', function($scope,$location, LoginService) {
	var self = this;
    self.user={id:null,username:'',password:'',priviledge:''};
	
    $scope.login = function(username,password){
    	LoginService.login(username,password)
	              .then(
	            		  $location.path('/WEB-INF/views/UserManagement'),      		  
			              function(errResponse){
				               console.error('Error while loging in.');
			              }	
            );
    };

}]);