'use strict';

App.controller('ComponentController', ['$scope', 'ComponentService', function($scope, ComponentService) {
          var self = this;
          self.component={idcomponent:null,name:'',price:'',description:''};
          self.components=[];
              
          self.fetchAllComponents = function(){
              ComponentService.fetchAllComponents()
                  .then(
      					       function(d) {
      						        self.components = d;
      					       },
            					function(errResponse){
            						console.error('Error while fetching components');
            					}
      			       );
          };
           
          self.createComponent = function(component){
              ComponentService.createComponent(component)
		              .then(
                      self.fetchAllComponents, 
				              function(errResponse){
					               console.error('Error while creating component.');
				              }	
                  );
          };

         self.updateComponent = function(component, id){
              ComponentService.updateComponent(component, id)
		              .then(
				              self.fetchAllComponents, 
				              function(errResponse){
					               console.error('Error while updating component.');
				              }	
                  );
          };

         self.deleteComponent = function(id){
              ComponentService.deleteComponent(id)
		              .then(
				              self.fetchAllComponents, 
				              function(errResponse){
					               console.error('Error while deleting component.');
				              }	
                  );
          };

          self.fetchAllComponents();

          self.submit = function() {
              if(self.component.idcomponent==null){
                  console.log('Saving New Component', self.component);    
                  self.createComponent(self.component);
              }else{
                  self.updateComponent(self.component, self.component.idcomponent);
                  console.log('Component updated with id ', self.component.idcomponent);
              }
              self.reset();
          };
              
          self.edit = function(id){
              console.log('id to be edited', id);
              for(var i = 0; i < self.components.length; i++){
                  if(self.components[i].id == id) {
                     self.component = angular.copy(self.components[i]);
                     break;
                  }
              }
          };
              
          self.remove = function(id){
              console.log('id to be deleted', id);
              for(var i = 0; i < self.components.length; i++){
                  if(self.components[i].id == id) {
                     self.reset();
                     break;
                  }
              }
              self.deleteComponent(id);
          };

          
          self.reset = function(){
              self.component={idcomponent:null,name:'',price:'',description:''};
              $scope.myForm.$setPristine(); //reset Form
          };

      }]);
