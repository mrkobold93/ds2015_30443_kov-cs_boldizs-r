'use strict';

App.factory('ComponentService', ['$http', '$q', function($http, $q){

	return {
		
			fetchAllComponents: function() {
					return $http.get('http://localhost:8080/dsfinal/component/getAll/')
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while fetching components');
										return $q.reject(errResponse);
									}
							);
			},
		    
		    createComponent: function(component){
					return $http.post('http://localhost:8080/dsfinal/component/create', component)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while creating component');
										return $q.reject(errResponse);
									}
							);
		    },
		    
		    updateComponent: function(component, id){
					return $http.put('http://localhost:8080/dsfinal/component/update'+id, component)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while updating component');
										return $q.reject(errResponse);
									}
							);
			},
		    
			deleteComponent: function(id){
					return $http.post('http://localhost:8080/dsfinal/component/delete',id)
							.then(
									function(response){
										return response.data;
									}, 
									function(errResponse){
										console.error('Error while deleting component');
										return $q.reject(errResponse);
									}
							);
			}
	};

}]);
