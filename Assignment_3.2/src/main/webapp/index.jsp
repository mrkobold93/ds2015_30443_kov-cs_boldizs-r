<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <title>Mr.Kobold - Assignment 3.2</title>
<body>
<h2>Awesome DVD Creation Platform!</h2>
<table>
    <form action="insert" method="post">
        <tr>
            <td>Title:</td>
            <td><input type="text" name="dvd_title" id="dvd_title"/></td>
        </tr>
        <tr>
            <td>Year:</td>
            <td><input type="text" name="dvd_year" id="dvd_year"/></td>
        </tr>
        <tr>
            <td>Price:</td>
            <td><input type="text" name="dvd_price" id="dvd_price"/></td>
        </tr>
        <tr>
            <td/>
            <td><input type="submit" value="Create!" name="dvd_create" id="dvd_create"/></td>
        </tr>
    </form>
</table>
</body>
</html>
