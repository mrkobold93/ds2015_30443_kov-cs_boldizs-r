package backend;

import common.DVD;

import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.Properties;

/**
 * Created by Mr Kobold on 12/3/2015.
 */
public class MailSender {
    private static String[] to = new String[] { "kovacsboldizsar@gmail.com",
                                                "zmxncbvqpwoeiruty312@gmail.com"};

    public static void sendMail(DVD dvd) {

        Properties props = new Properties();
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.socketFactory.class",
                "javax.net.ssl.SSLSocketFactory");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "587");
        Session session = Session.getDefaultInstance(props,
                new javax.mail.Authenticator() {
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("zmxncbvqpwoeiruty312@gmail.com","mrkobold93");
                    }
                });

        for (int i = 0; i < to.length; i++) {
            try {
                Message message = new MimeMessage(session);
                message.setFrom(new InternetAddress("zmxncbvqpwoeiruty312@gmail.com"));
                message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to[i]));
                message.setSubject("New DVD!!!");
                message.setText("Hello Man! \nThere's a new DVD :)\n" + dvd.toString());
                Transport.send(message);
            } catch (Exception e) {
                throw new RuntimeException(e);
            }
        }
    }
}
