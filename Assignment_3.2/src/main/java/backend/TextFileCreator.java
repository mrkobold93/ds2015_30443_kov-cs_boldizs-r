package backend;

import common.DVD;

import java.io.File;
import java.io.PrintWriter;

/**
 * Created by Mr Kobold on 12/3/2015.
 */
public class TextFileCreator {
    public static void createTextFile(DVD dvd) {
        // generate new file's name
        String newFileName = dvd.getTitle() + ".txt";

        File muvi = new File(newFileName);

        try {
            PrintWriter printWriter = new PrintWriter(muvi, "UTF-8");
            printWriter.print("Movie: title - " + dvd.getTitle() +
                    "\n       year - " + dvd.getYear() +
                    "\n       price - " + dvd.getPrice());
            printWriter.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
