package ro.tuc.dsrl.ds.handson.assig.three.producer.start;

import ro.tuc.dsrl.ds.handson.assig.three.producer.connection.QueueServerConnection;

import java.io.IOException;

import common.DVD;

/**
 * @Author: Technical University of Cluj-Napoca, Romania
 *          Distributed Systems, http://dsrl.coned.utcluj.ro/
 * @Module: assignment-one-client
 * @Since: Sep 1, 2015
 * @Description:
 *	Starting point for the Producer Client application. This
 *	application will send several messages to be inserted
 *  in the queue server (i.e. to be sent via email by a consumer).
 */
public class ClientStart {
	private static final String HOST = "localhost";
	private static final int PORT = 8888;

	private ClientStart() {
		
	}

	public static void main(String[] args) {
		QueueServerConnection queue = new QueueServerConnection(HOST, PORT);

		try {
			
			DVD newDVD = new DVD("Harry Pottah", 2000, 10.5);
			queue.writeMessage(newDVD);
			
			newDVD = new DVD("LOTR", 2001, 20.5);
			queue.writeMessage(newDVD);
			
			newDVD = new DVD("NewMovie", 2001, 20.5);
			queue.writeMessage(newDVD);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
