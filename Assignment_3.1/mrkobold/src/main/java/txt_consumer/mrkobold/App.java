package txt_consumer.mrkobold;

import java.io.PrintWriter;

import connection.QueueServerConnection;

/**
 * Hello world!
 *
 */
public class App {
	private static PrintWriter writer;
	
    private App() throws Exception {
		
    }
    
    public static void main(String[] args) {
    	QueueServerConnection queue = new QueueServerConnection("localhost", 8888);
    
    	Object message;
    	
    	while(true) {
    		try {
    			message = queue.readMessage();
    			writer = new PrintWriter("muuviz.txt", "UTF-8");
    			writer.println(message.toString());
    			
    			writer.close();
    			
    		} catch (Exception ex) {
    			//writer.close();
    			ex.printStackTrace();
    		}
    	}
    }
    
}
