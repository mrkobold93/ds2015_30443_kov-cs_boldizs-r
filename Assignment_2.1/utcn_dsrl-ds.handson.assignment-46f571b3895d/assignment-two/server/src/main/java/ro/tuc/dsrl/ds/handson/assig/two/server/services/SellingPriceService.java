package ro.tuc.dsrl.ds.handson.assig.two.server.services;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;
import ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces.ISellingPriceService;

/**
 * Created by Mr Kobold on 11/11/2015.
 */
public class SellingPriceService implements ISellingPriceService{

    @Override
    public double computeSellingPrice(Car c) {

        // check that the production year is inside normal limits
        if (c.getYear() < 1980 || c.getYear() > 2014) {
            throw new IllegalArgumentException("The car's production year must be from the interval [1980,2014]");
        }

        // if car is okay, then return the double value of the estimated selling price
        return c.getPurchasingPrice() - c.getPurchasingPrice()/7*(2015-c.getYear());
    }
}
