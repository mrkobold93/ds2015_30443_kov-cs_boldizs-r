package ro.tuc.dsrl.ds.handson.assig.two.common.serviceinterfaces;

import ro.tuc.dsrl.ds.handson.assig.two.common.entities.Car;

/**
 * Created by Mr Kobold on 11/11/2015.
 */
public interface ISellingPriceService {
    /**
     * Computer selling price based on
     * @param c
     * @return
     */
    double computeSellingPrice(Car c);
}
